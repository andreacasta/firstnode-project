import { NextFunction, Request, Response } from "express";
import { idText } from "typescript";
import app from "../app";
import { AppDataSource } from "../db";
import { User } from "../entities/User";
import {
  httpExeption,
  BadRequestExeption,
  notFound,
} from "../exeptions/httpExeption";
import Logger from "../lib/logger";

import userService from "./userService";
import jwt from "jsonwebtoken";
import config from "../config/config";

function createToken(user: User) {
  return jwt.sign({ id: user.id, username: user.username }, config.jwtSecret, {
    expiresIn: "90d",
  });
}

// nel repository si trova il file d'espostazione per fare le chiamate postman

// POST USERS (CREATE USERS)
export const createUsers = async (req: Request, res: Response) => {
  const user = await userService.createOneUser(req.body);

  return res
    .status(200)
    .json(
      `Utente Creato, nome: ${req.body.fistname}, cognome:${req.body.lastname}`
    );
};

// GET ALL USERS
export const getUsers = async (req: Request, res: Response) => {

  const users = await userService.getAllUsers();
  Logger.error("This is an error log");
  Logger.warn("This is a warn log");
  Logger.info("This is a info log");
  Logger.http("This is a http log");
  Logger.debug("This is a debug log");

  return res.json(users);
};

// UPDATE USERS
export const updateUser = async (req: Request, res: Response) => {
  const user = await userService.updateOneUser(req.params.id, req.body);

  return res.json(
    `Utente modificato, nome: ${req.body.fistname}, cognome:${req.body.lastname}`
  );
};

// DELETE USERS

export const deleteUser = async (req: Request, res: Response) => {
  const user = await userService.deleteOneUser(req.params.id);

  return res.json(`utente eliminato  ${req.params.id} `);
};

// GET one user

export const getUser = async (req: Request, res: Response) => {
  const user = await userService.getOneUsers(req.params.id);

  return res.json(
    `Utente selezionato, nome: ${user.fistname}, cognome:${user.lastname}, id:${req.params.id},  active: ${user.active} `
  );
};

// POST LOGIN USER (CREATE USERS)
export const loginUser = async (req: Request, res: Response) => {
  const user = await userService.loginOneUser(req.body);

  return res.status(200).json({ token: createToken(user) });
};
