import { AnyRecord } from "dns";
import { Request, Response } from "express";
import { Any } from "typeorm";
import { ArrayTypeNode } from "typescript";
import { AppDataSource } from "../db";
import { User } from "../entities/User";
import {
  httpExeption,
  BadRequestExeption,
  notFound,
} from "../exeptions/httpExeption";
import bcrypt from "bcrypt";
import Logger from "../lib/logger";

class UserService {
  // GET ALL USERS

  async getAllUsers() {
    const result = await AppDataSource.transaction(
      async (transactionalEntityManager) => {
        const users = await transactionalEntityManager.find(User);
        return users;
      }
    );
    return result;
  }
  // GET ONE USER
  async getOneUsers(id: String) {
    const result = await AppDataSource.transaction(
      async (transactionalEntityManager) => {
        const user = await transactionalEntityManager.findOneBy(User, {
          id: Number(id),
        });
        if (!user) {
          throw new notFound();
        } else {
          return user;
        }
      }
    );
    return result;
  }

  // CREATE ONE USER
  async createOneUser(body: any) {
    const { fistname, lastname, username, password } = body;

    if (!fistname && !lastname && !username) {
      throw new BadRequestExeption();
    }
    const hash = await bcrypt.hash(password, 10);
    const user = await AppDataSource.transaction(
      async (transactionalEntityManager) => {
        const userDb = await transactionalEntityManager.findOne(User, {
          where: { username: username },
        });
        if (userDb) {
          throw new httpExeption(`Username: ${username}, gia esistente`, 400);
        } else {
          return await transactionalEntityManager.save(User, {
            fistname,
            lastname,
            username,
            password: hash,
          });
        }
      }
    );
    if (user) {
      return user;
    } else {
      throw new httpExeption(`utente non creato`, 400);
    }
  }

  // UPDATE ONE USER
  async updateOneUser(id: any, body: any) {
    const { fistname, lastname, username } = body;
    const result = await AppDataSource.manager.transaction(
      async (transactionalEntityManager) => {
        const user = await User.findOneBy({ id: id });

        if (!user) {
          throw new notFound();
        } else {
          await transactionalEntityManager.update(
            User,
            {
              fistname,
              lastname,
              username,
            },
            { id: id }
          );

          return user;
        }
      }
    );

    return result;
  }

  // DELETE ONE USER
  async deleteOneUser(id: String) {
    const result = await AppDataSource.transaction(
      async (transactionalEntityManager) => {
        const user = await transactionalEntityManager.delete(User, {
          id: Number(id),
        });

        if (user.affected === 0) {
          throw new notFound();
        } else {
          console.log("user deleted");
          return user;
        }
      }
    );
    return result;
  }

  // POST LOGIN ONE USER
  async loginOneUser(body: any) {
    const { username, password } = body;

    if (!username && !password) {
      throw new BadRequestExeption();
    }

    const user: any = await AppDataSource.transaction(
      async (transactionalEntityManager) => {
        const userDb: any = await transactionalEntityManager.findOne(User, {
          where: { username: username },
        });
        if (userDb) {
          const validPass: any = await bcrypt.compare(
            password,
            userDb.password
          );
          if (validPass) {
            return validPass;
          } else {
            throw new httpExeption(`wrong password!`, 400);
          }
        }
      }
    );
    if (user) {
      return user;
    } else {
      throw new httpExeption(`Error, not logged`, 401);
    }
  }
}

export default new UserService();
