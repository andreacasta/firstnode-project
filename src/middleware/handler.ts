import { Request, Response, NextFunction } from "express";
// questo va aggiunto su app.ts
export const routeAsyncHandler =
  (f: any) => (req: Request, res: Response, next: NextFunction) => {
    f(req, res)
      .then(() => {
        next();
      })
      .catch((err: any) => {
        console.log("errore", err);
        next(err);
      });
  };

export function errorMiddlerwareCatchExpetion(
  error: any,
  _request: Request,
  response: Response,
  _next: NextFunction
) {
  const date = error.date;
  const message = error.message || "an unexpeted expetion occured";
  const status = error.status || 500;
  const code = error.code || 1;

  response.status(status).json({
    code,
    message,
    date,
  });
}
