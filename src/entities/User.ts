import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  BaseEntity,
} from "typeorm";

// extends server per chiamare la classe user e poterla ussare per fare il crud
@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fistname: string;

  @Column()
  lastname: string;

  @Column({
    default: true,
  })
  active: boolean;

  @CreateDateColumn()
  creatAt: Date;

  @UpdateDateColumn()
  updatedAd: Date;

  @Column()
  username: string;

  @Column()
  password: string;
}
