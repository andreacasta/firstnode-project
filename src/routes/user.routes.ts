import { Router } from "express";
import {
  createUsers,
  deleteUser,
  getUser,
  getUsers,
  updateUser,
} from "../controllers/users.controllers";
import { routeAsyncHandler } from "../middleware/handler";
import passport from "passport";

const router = Router();

router.get(
  "/users",
  passport.authenticate("jwt", { session: false }),
  routeAsyncHandler(getUsers)
);

router.put(
  "/users/:id",
  passport.authenticate("jwt", { session: false }),
  routeAsyncHandler(updateUser)
);

router.delete(
  "/users/:id",
  passport.authenticate("jwt", { session: false }),
  routeAsyncHandler(deleteUser)
);

// get single user
router.get(
  "/users/:id",
  passport.authenticate("jwt", { session: false }),
  routeAsyncHandler(getUser)
);

export default router;
