import { Router } from "express";
import { createUsers, loginUser } from "../controllers/users.controllers";
import { routeAsyncHandler } from "../middleware/handler";

const router = Router();

router.post("/register", routeAsyncHandler(createUsers));

router.post("/login", routeAsyncHandler(loginUser));

export default router;
