import express, { NextFunction } from "express";
import morgan from "morgan";
import cors from "cors";
import userRoutes from "./routes/user.routes";
import authRoutes from "./routes/auth.router";

import { errorMiddlerwareCatchExpetion } from "./middleware/handler";
import "reflect-metadata";
import Logger from "./lib/logger";
import morganMiddleware from "./config/morganMiddleware";

import passport from "passport";
import "reflect-metadata";
import passportMiddleware from "./middleware/passport";

const app = express();
app.use(morganMiddleware);

// app.use(morgan("dev")); // more information for developer show the info in console

app.use(cors());

app.use(express.json());

// app.use((req, res, next) => {
//   Logger.debug("Middleware 1 called.");
//   next(); // calling next middleware function or handler
// });

app.use(authRoutes);
app.use(userRoutes);

app.use(errorMiddlerwareCatchExpetion);

app.use(passport.initialize());
passport.use(passportMiddleware);

// app.use((err: any, req: Request,res: Response, _next: NextFunction) => {
//     console.error(err);
//      // calling next middleware function or handler
//     _next()
// });

export default app;
