export class httpExeption extends Error {
  message: string;
  status: number;
  constructor(message: string, status?: number) {
    super();
    this.message = message;
    this.status = status ? status : 500;
  }
}

export class BadRequestExeption extends Error {
  message: string;
  status: number;
  constructor() {
    super();
    this.message = "bad request";
    this.status = 400;
  }
}

export class notFound extends Error {
  message: string;
  status: number;
  constructor() {
    super();
    this.message = "not found";
    this.status = 404;
  }
}
