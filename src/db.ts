import { DataSource } from "typeorm";
import { User } from "./entities/User";
require("dotenv").config({
  path: "/Users/andreacasta/Desktop/ANOKI/firstnode-project/.env",
});
console.log(process.env.DB_USER);

export const AppDataSource = new DataSource({
  type: "postgres",
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PSW,
  database: "CRUD",
  entities: [User],
  synchronize: false,
  logging: false,
});
