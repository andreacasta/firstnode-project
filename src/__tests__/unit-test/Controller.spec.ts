import { Request, Response } from "express";
import {
  getUsers,
  getUser,
  createUsers,
  updateUser,
  deleteUser,
} from "../../controllers/users.controllers";
import { jest } from "@jest/globals";
import { httpExeption, notFound } from "../../exeptions/httpExeption";
import UserService from "../../controllers/userService";
import { User } from "../../entities/User";

const request = {
  params: { id: 12 },
  body: {},
} as unknown as Request;

const user = {
  id: 12,
  fistname: "luigi",
  lastname: "verdi",
  active: true,
  creatAt: "2022-06-23T07:24:52.695Z",
  updatedAd: "2022-06-23T07:24:52.695Z",
} as any;

const request2 = {
  params: { id: 12 },
  body: {},
} as unknown as Request;

// request for create
const request3 = {
  params: {},
  body: { fistname: "nome prova", lastname: "cognome prova" },
} as unknown as Request;

// response  for create
const userCreate = {
  id: 12,
  fistname: "nome prova",
  lastname: "cognome prova",
  active: true,
  creatAt: "2022-06-23T07:24:52.695Z",
  updatedAd: "2022-06-23T07:24:52.695Z",
} as any;

// response for update
const userUpdate = {
  id: 13,
  fistname: "luigi",
  lastname: "verdi",
  active: true,
  creatAt: "2022-06-23T07:24:52.695Z",
  updatedAd: "2022-06-23T07:24:52.695Z",
} as any;

// request for update
const request4 = {
  params: { id: 13 },
  body: { fistname: "luigi", lastname: "verdi" },
} as unknown as Request;

// GET ALL USERS
describe.skip("test user controller", () => {
  test("GET ALL USERS - return with all users", async () => {
    const response: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return response;
      }),
    };

    const expectedUsers = [
      {
        id: 11,
        fistname: "mario",
        lastname: "rossi",
        active: true,
        creatAt: "2022-06-23T07:24:46.932Z",
        updatedAd: "2022-06-23T07:24:46.932Z",
      },
      {
        id: 12,
        fistname: "luigi",
        lastname: "verdi",
        active: true,
        creatAt: "2022-06-23T07:24:52.695Z",
        updatedAd: "2022-06-23T07:24:52.695Z",
      },
    ] as any;

    jest.spyOn(UserService, "getAllUsers").mockResolvedValue(expectedUsers);

    await getUsers(request, response);

    expect(response.json.mock.calls.length).toBe(1);
    expect(response.json).toBeCalledWith(expectedUsers);
  });

  // GET ONE USER BY ID
  test("GET ONE USER - return one users by ID", async () => {
    const response: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return response;
      }),
    };

    jest.spyOn(UserService, "getOneUsers").mockResolvedValue(user);

    await getUser(request, response);
    expect(response.json.mock.calls.length).toBe(1);
    expect(response.json).toBeCalledWith(
      `Utente selezionato, nome: ${user.fistname}, cognome:${user.lastname}, id:12,  active: ${user.active} `
    );
  });

  // NOT GET ONE USER BY ID
  it("GET ONE USER - ERROR", async () => {
    const res: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return res;
      }),
    };

    jest.spyOn(UserService, "getOneUsers").mockImplementation(() => {
      throw new notFound();
    });
    try {
      await getUser(request, res);
    } catch (e) {
      if (e instanceof notFound) {
        expect(e.message).toBe("not found");
      }
      expect(e).toStrictEqual(new notFound());
      expect(res.status.mock.calls.length).toBe(0);
      expect(res.status.mock.calls.length).toBe(0);
    }
  });

  // NOT GET ONE USER BY ID
  // test("return a error - one users by ID", async () => {
  //   let responseObject = {} as unknown;

  //   const response: any = {
  //     json: jest.fn().mockImplementation((result) => {
  //       responseObject = result;
  //     }),
  //   };
  //   const expectedUsers = [
  //     {
  //       fistname: "luigi",
  //       lastname: "verdi",
  //       active: true,
  //       creatAt: "2022-06-22T10:40:06.061Z",
  //       updatedAd: "2022-06-22T10:40:06.061Z",
  //     },
  //   ];

  //   await getUser(request2, response);
  //   expect(response.json.mock.calls.length).toBe(1);
  // });

  // CREATE one USER
  test("CREATE A USER", async () => {
    const res: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return res;
      }),
    };

    // const error = new httpExeption("Utente gia esistente");
    jest.spyOn(UserService, "createOneUser").mockResolvedValue(userCreate);

    await createUsers(request3, res);
    expect(res.status).toBeCalledWith(200);
    expect(res.json.mock.calls.length).toBe(1);
    expect(res.json).toBeCalledWith(
      `Utente Creato, nome: ${userCreate.fistname}, cognome:${userCreate.lastname}`
    );
  });

  // NOT GET ALL USER BY ID
  it("CREATE A USER - ERROR", async () => {
    const res: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return res;
      }),
    };

    jest.spyOn(UserService, "createOneUser").mockImplementation(() => {
      throw new httpExeption(`Utente gia esistente`, 400);
    });
    try {
      await createUsers(request, res);
    } catch (e) {
      if (e instanceof httpExeption) {
        expect(e.message).toBe("Utente gia esistente");
      }
      expect(e).toStrictEqual(new httpExeption(`Utente gia esistente`, 400));
      expect(res.status.mock.calls.length).toBe(0);
      expect(res.status.mock.calls.length).toBe(0);
    }
  });

  // UPDATE ONE USER
  test("UPDATE ONE USER", async () => {
    const res: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return res;
      }),
    };

    // const error = new httpExeption("Utente gia esistente");
    jest.spyOn(UserService, "updateOneUser").mockResolvedValue(userUpdate);

    await updateUser(request4, res);
    expect(res.json.mock.calls.length).toBe(1);
    expect(res.json).toBeCalledWith(
      `Utente modificato, nome: ${userUpdate.fistname}, cognome:${userUpdate.lastname}`
    );
  });

  // NOT UPDATE ONE USER
  it("UPDATE ONE USER - ERROR", async () => {
    const res: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return res;
      }),
    };

    jest.spyOn(UserService, "updateOneUser").mockImplementation(() => {
      throw new notFound();
    });
    try {
      await updateUser(request, res);
    } catch (e) {
      if (e instanceof notFound) {
        expect(e.message).toBe("not found");
      }
      expect(e).toStrictEqual(new notFound());
      expect(res.status.mock.calls.length).toBe(0);
      expect(res.status.mock.calls.length).toBe(0);
    }
  });

  // DELETE ONE USER BY ID
  test("DELETE ONE USER", async () => {
    const response: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return response;
      }),
    };

    jest.spyOn(UserService, "deleteOneUser").mockResolvedValue(user);

    await deleteUser(request, response);
    expect(response.json.mock.calls.length).toBe(1);
    expect(response.json).toBeCalledWith(
      `utente eliminato  ${request.params.id} `
    );
  });

  // NOT DELETE ONE USER
  it("DELETE ONE USER - ERROR", async () => {
    const res: any = {
      send: jest.fn(),
      json: jest.fn(),
      status: jest.fn(() => {
        return res;
      }),
    };

    jest.spyOn(UserService, "updateOneUser").mockImplementation(() => {
      throw new notFound();
    });
    try {
      await updateUser(request, res);
    } catch (e) {
      if (e instanceof notFound) {
        expect(e.message).toBe("not found");
      }
      expect(e).toStrictEqual(new notFound());
      expect(res.status.mock.calls.length).toBe(0);
      expect(res.status.mock.calls.length).toBe(0);
    }
  });
});

// interfaccia della request (stubinterface ) -> mocka
// cercare uno simile in jest

//expect error
