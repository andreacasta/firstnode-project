import app from "../../app";
import { AppDataSource } from "../../db";
import request from "supertest";
import supertest from "supertest";

let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NTY5MzE3MzIsImV4cCI6MTY2NDcwNzczMn0.5ApE6cgcsPNVa4vKfanr2a1NdkHhZavmo-42VeJWSuM';

const testUser = {
  id: 4,
  username: "andreacasta",
  fistname: "andrea",
  lastname: "castaldello",
  active: true,
  password: "admin",
};

const registerUser = {
  id: 4,
  username: "andreacasta",
  fistname: "andrea",
  lastname: "castaldello",
  password: "admin"
};

const NotLoginUser = {
  id: 4,
  username: "andrea",
  fistname: "andrea",
  lastname: "castaldello",
  password: "admin"
};

const testUpdateUser = {

  username: "andreacasta",
  fistname: "luca",
  lastname: "castaldello",
  password: "admin"
};



describe("unite test user", () => {
  beforeAll(async() => {
   await AppDataSource.initialize().then(() => {
    console.log("data soruce has been initized!")
   }).catch(e => {
    console.log("e:",e)
   })
    
  });

  afterAll(() => {
    AppDataSource.destroy();
  });

  //GET ALL USERS
test("getAll USER", async () => {

  const response = await request(app)
      .get("/users")
      .auth(token, {type: "bearer"})
      .set("Accept","application/json")
  expect(response.status).toEqual(200)
 })

//REGISTER USER
test("register a user", async () => {
    const response = await request(app).post("/register").send(registerUser);
    expect(response.statusCode).toBe(200);
  });


   //LOGIN USER
   test("login a user", async () => {
    const response = await request(app).post("/login").send(testUser);
    expect(response.statusCode).toBe(200);
  });

  

  // NOT LOGIN USER
  test("Not login", async () => {
    const response = await request(app).post("/login").send(NotLoginUser);
    expect(response.statusCode).toBe(401);
    expect(response.body.errors).not.toBeNull();
    expect(response.body).toEqual({ code: 1, message: "Error, not logged" });
  });

  //GET ONE USER

  test("should be get a one user", async () => {

    const response = await request(app)
        .get("/users/4")
        .auth(token, {type: "bearer"})
        .set("Accept","application/json")
    expect(response.status).toEqual(200)
    expect(response.body).toEqual(
      `Utente selezionato, nome: ${testUser.fistname}, cognome:${testUser.lastname}, id:${testUser.id},  active: ${testUser.active} `
    );
   })


  //NOT GET ONE USER
  test("should be not get a one user", async () => {
    const response = await request(app)
        .get("/users/400")
        .auth(token, {type: "bearer"})
        .set("Accept","application/json")
    expect(response.status).toEqual(404)
    expect(response.body.errors).not.toBeNull();
    expect(response.body).toEqual({ code: 1, message: "not found" });
  });

  // UPDATE USER
  test("should be update a one user", async () => {
    const response = await request(app)
        .put("/users/4")
        .auth(token, {type: "bearer"})
        .set("Accept","application/json")
        .send(testUpdateUser);
    expect(response.status).toEqual(200)
    expect(response.body).toEqual(
      `Utente modificato, nome: ${testUpdateUser.fistname}, cognome:${testUpdateUser.lastname}`
    );
  });

  // NOT UPDATE USER
  test("should be NOT update a one user", async () => {
    const response = await request(app)
    .put("/users/300")
    .auth(token, {type: "bearer"})
    .set("Accept","application/json")
    .send(testUpdateUser);
    expect(response.status).toEqual(404)
    expect(response.body).toEqual({ code: 1, message: "not found" });
  });

  // DELETE USER
  test("should be delete one user", async () => {
    const response = await request(app)
    .delete("/users/2")
    .auth(token, {type: "bearer"})
    .set("Accept","application/json")
    expect(response.statusCode).toBe(200);
  });

  //NOT DELETE USER
  test("should be delete one user", async () => {
    const response = await request(app)
    .delete("/users/300")
    .auth(token, {type: "bearer"})
    .set("Accept","application/json")
    expect(response.statusCode).toBe(404);
    expect(response.body).toEqual({ code: 1, message: "not found" });
  });


});
