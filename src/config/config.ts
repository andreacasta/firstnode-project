require("dotenv").config({
  path: "/Users/andreacasta/Desktop/ANOKI/firstnode-project/.env",
});

export default {
  jwtSecret: process.env.JWT_SECRET || "somesecrettoken",
};
