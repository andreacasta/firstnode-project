import "reflect-metadata";
import app from "./app";
import { AppDataSource } from "./db";
require("dotenv").config({
  path: "/Users/andreacasta/Desktop/ANOKI/firstnode-project/.env",
});

AppDataSource.initialize()
  .then(() => {
    console.log(`Database Connected on PORT: ${process.env.DB_PORT}`);
  })
  .catch((error: Error) => {
    console.error(error);
  });

app.listen(3001, () => {
  console.log(`server is running and lissening on ${process.env.PORT} `);
});

