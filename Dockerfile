FROM node:16.13.0

WORKDIR /usr

RUN npm install -g typescript

COPY package.json ./

RUN npm install

COPY . ./


CMD ["npm","run","dev"]


EXPOSE 3000

# docker build -t node
# docker run -p 8080:3000 node 